function App() {
  const title = 'Blog post'
  const body = 'This is my blog post'
  const comments = [
      {
          id:1,
          text:'comment 1'
      },
      {
        id:2,
        text:'comment 2'
      },
      {
        id:3,
        text:'comment 3'
      },
  ]

  const showComments = true

  const commenBlock = (
    <div className="comment">
    <h1> comment section {comments.length}</h1>
        <ul>
            {comments.map((comment,index) => (
                   <li key={index}>{comment.text}</li>
            ))}
        </ul>
    </div>
  )


return (   
<div className="container">
    <h1> Hello from main componenet </h1>
     <h2>{title}</h2>
    <p>{body}</p>
 {showComments && commenBlock}
</div>  
)}
export default App